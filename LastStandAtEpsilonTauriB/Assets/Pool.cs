﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

public class PoolTracker
{
    public List<PoolData> Pools = new List<PoolData>();
    public PoolTracker()
    {
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
    }
    
    private void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        Pools = new List<PoolData>();
    }

    [System.Serializable]
    public class PoolData
    {
        public GameObject Prefab;
        public List<GameObject> Pool = new List<GameObject>();
        public GameObject GetFromPoolOrInstantiate(bool force = false)
        {
            var existing = Pool.FirstOrDefault(p => p.activeSelf == false);
            if (!existing || force)
            {
                existing = GameObject.Instantiate(Prefab, null);
                existing.SetActive(false);
                Pool.Add(existing);
            }
            return existing;
        }
    }
    private static PoolTracker _instance;
    public static PoolTracker Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PoolTracker();
            }
            return _instance;
        }
    }
    private PoolData _make(GameObject prefab)
    {
       var  pool = new PoolData()
        {
            Prefab = prefab
        };
        Pools.Add(pool);
        return pool;
    }
    private PoolData _add(PoolData pool, int count)
    {
        for(int i = 0; i < count; i++)
        {
            pool.GetFromPoolOrInstantiate(true); 
        }
        return pool;
    }
    public static PoolData Make(GameObject prefab, int count)
    {
        return Instance._add(Instance._getPool(prefab) ?? Instance._make(prefab),count);
    }
    public static GameObject Get(GameObject prefab, bool force = false)
    {
        return Instance._get(prefab,force);
    }
    private PoolData _getPool(GameObject prefab)
    {
        return Pools.FirstOrDefault(x => x.Prefab == prefab) ?? _make(prefab); 
    }
    private GameObject _get(GameObject prefab, bool force = false)
    {
        
        return _getPool(prefab).GetFromPoolOrInstantiate(force);
    }
}
