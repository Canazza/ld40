﻿using UnityEngine;
using System.Collections;
using System;

public interface INewtonianAnimator
{ 
    float speed { set; }
    float rotation { set; }
}

public class NewtonianAnimator : MonoBehaviour, INewtonianAnimator
{
    public Animator animator; 
    public string Animator_Speed_Float;
    public string Animator_Rotation_Float;
     

    public float speed { set
        {
            animator.SetFloat(Animator_Speed_Float, value);
        }
    }
    public float rotation {
    set
        {
            animator.SetFloat(Animator_Rotation_Float, value);
        }
    }
}