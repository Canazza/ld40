﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Duplicator : MonoBehaviour {
    public UnityEvent OnDuplicated, OnPostDuplicate;
    public void Duplicate()
    {
        StartCoroutine(doDupe());

    }
    private IEnumerator doDupe()
    {
        var dupe = Instantiate(this.gameObject, this.transform.parent);
        this.transform.SetParent(null);
        dupe.name = this.name;
        yield return null;
        dupe.GetComponent<Duplicator>().OnDuplicated.Invoke();
        OnPostDuplicate.Invoke();

    }
}
