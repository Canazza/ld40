﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedShooter : MonoBehaviour {
    public float MinInterval, MaxInterval;
    public GameObject Prefab;
    public AudioClip PlayOnFire;
    private void Start()
    {
        PoolTracker.Make(Prefab, 2);
    }
    void OnEnable()
    {
        StartCoroutine(DoIntervals());
    }
    IEnumerator DoIntervals()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(MinInterval, MaxInterval));
            Shoot();
        }
    }
    void Shoot()
    {
        GameData.PlayClip(PlayOnFire);
        var spawned = PoolTracker.Get(Prefab);
        spawned.transform.position = this.transform.position;
        spawned.transform.rotation = this.transform.rotation;
        spawned.SetActive(true);
    }
}
