﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour {
    public string InputButton;
    public float Ammo = 1;
    public float RefireTime = 0;
    public float ReloadTime = 0.5f;
    public GameObject[] PortsInSequence;
    public GameObject Prefab;
    public List<GameObject> Pool;
    public int CreateOnStart;
    public bool InheritVelocity;
    public GameObject VelocityFrom;
    public string FromVelocityProperty;
    public string ToVelocityProperty;
    private FieldInfo FromVelocityPropertyInfo,ToVelocityPropertyInfo;
    private Component VelocityFromComponent;
    public FloatEvent OnReload01;
    public UnityEvent OnFire;
    private void Awake()
    {
        PoolTracker.Make(Prefab,CreateOnStart);
    }

    private void SetVelocity(GameObject Prefab)
    {
        var componentBits = ToVelocityProperty.Split('.');
        var prefabComponent = Prefab.GetComponent(componentBits[0]);
        if (FromVelocityPropertyInfo == null)
        {
            var VelocityFromBits = FromVelocityProperty.Split('.');
            VelocityFromComponent = VelocityFrom.GetComponent(VelocityFromBits[0]);
            FromVelocityPropertyInfo = VelocityFromComponent.GetType().GetField(VelocityFromBits[1]);
        }
        if (ToVelocityPropertyInfo == null)
        {
            ToVelocityPropertyInfo = prefabComponent.GetType().GetField(componentBits[1]);
        }
        Vector2 value = (Vector2)FromVelocityPropertyInfo.GetValue(VelocityFromComponent);
        ToVelocityPropertyInfo.SetValue(prefabComponent, value);

    }

    public bool IsFiring = false;
	// Update is called once per frame
	void Update () {
		if(!IsFiring && Input.GetButton(InputButton))
        {
            StartCoroutine(Fire());
        }
	}
    
    IEnumerator Fire()
    {
        if (!IsFiring)
        {
            IsFiring = true;
            OnReload01.Invoke(0);
            OnFire.Invoke();
            foreach (GameObject Port in PortsInSequence)
            {
                if (!Port.activeSelf) continue;
                var spawned = PoolTracker.Get(Prefab);
                spawned.SetActive(true);
                spawned.transform.position = Port.transform.position;
                spawned.transform.rotation = Port.transform.rotation;

                if(InheritVelocity) SetVelocity(spawned);
                
                if (RefireTime > 0) yield return new WaitForSeconds(RefireTime);
            }
            if (ReloadTime > 0)
            {
                for(float t = 0; t < 1; t += Time.deltaTime / ReloadTime)
                {
                    OnReload01.Invoke(t);
                    yield return null;
                }
            }
            OnReload01.Invoke(1);
            IsFiring = false;
        }
    }
}
