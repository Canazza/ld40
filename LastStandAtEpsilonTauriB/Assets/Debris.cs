﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : ComputerControlled {
    public Vector2 Velocity;
    public float RotationVelocity = 1;
    public bool RandomDirection = false;
    public float ScreenWrapPadding = 0.1f;
    public LayerMask ImpactMask;
    private PolygonCollider2D c;
    public int DamageOnImpact = 10;

    private new void OnEnable()
    {
        base.OnEnable();
        if(c == null) c = this.GetComponent<PolygonCollider2D>();
        if(RandomDirection)
        {
            Velocity = Quaternion.Euler(0, 0, Random.Range(0f, 360f)) * Vector2.up * Velocity.magnitude;
        }
    }
    private void Update()
    {
        this.transform.Rotate(Vector3.back, RotationVelocity * Time.deltaTime);
        if (c != null)
        {
            var hit = Physics2D.OverlapCircle(this.transform.position, Mathf.Min(c.bounds.size.x, c.bounds.size.y) / 2, ImpactMask);
            if (hit)
            {
                var dmg = hit.GetComponent<IDamageable>();
                if (this.TakeDamage(1, DamageType.Impact))
                {
                    if (dmg != null) dmg.TakeDamage(DamageOnImpact, DamageType.Impact);
                    this.Velocity = -((Vector2)hit.transform.position - (Vector2)this.transform.position).normalized * this.Velocity.magnitude;
                }
            }
        }
    }
    private Vector2 wrapPosition;
    void FixedUpdate()
    {
        this.transform.Translate(Velocity * Time.fixedDeltaTime, Space.World);
        if(this.gameObject.WrapToCamera(Camera.main, out wrapPosition,ScreenWrapPadding))
        {
            this.transform.position = wrapPosition;
        }
    }
}
