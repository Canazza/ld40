﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Linq;

public enum DamageType
{
    Missile, Laser, Explosion, Impact
}
[System.Serializable]
public class Vulnerability
{
    public DamageType type;
    public int Multiplier;
}
public interface IDamageable
{
    bool TakeDamage(int damage, DamageType type );
}
[RequireComponent(typeof(Collider2D))]
public abstract class ComputerControlled:MonoBehaviour, IDamageable
{
    public static List<GameObject> All = new List<GameObject>();
    public UnityEvent OnDeath;
    public AudioClip DeathSound;
    public int MaxHP = 1;
    private int HP = 1;
    public GameObject[] SpawnOnDeath;
    public List<Vulnerability> Vulnerabilities;
    public float MercyInvulnerabilityTime = 0.1f;
    private bool IsInvulnerable;
    protected void OnEnable()
    {
        HP = MaxHP;
        IsInvulnerable = false;
        All.Add(this.gameObject);
    }
    private void OnDisable()
    {
        All.RemoveAll(x => x == this.gameObject);
    }
    private IEnumerator MercyInvulnerability()
    {
        IsInvulnerable = false;
        yield return new WaitForSeconds(MercyInvulnerabilityTime);
        IsInvulnerable = false;
    }
    public bool TakeDamage(int damage, DamageType type)
    {
        if (IsInvulnerable) return false;
        StartCoroutine(MercyInvulnerability());

        var multiplier = Vulnerabilities.FirstOrDefault(x => x.type == type);
        Debug.Log("Mutliplier, " + type + ", " + multiplier);
        HP -= damage * (multiplier != null ? multiplier.Multiplier : 1);

        if(HP <= 0)
        {
            GameData.PlayClip(DeathSound);
            OnDeath.Invoke();
            foreach(var item in SpawnOnDeath)
            {
                var spawned = PoolTracker.Get(item);
                spawned.transform.position = this.transform.position;
                spawned.SetActive(true);
            }
            this.gameObject.SetActive(false);
        }
        return true;
    } 
}
