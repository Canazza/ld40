﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Delay : MonoBehaviour {
    public float DelayTime;
    public UnityEvent OnDelay;
    private void OnEnable()
    {
        Invoke("DoDelay", DelayTime);
    }
    private void DoDelay()
    {
        OnDelay.Invoke();
    }
}
