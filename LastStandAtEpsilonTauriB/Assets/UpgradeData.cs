﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UpgradeData : MonoBehaviour {
    public string GameScreen = "Game";
    public Button BtnAddSpread, BtnAddMissile, BtnAddFan, BtnAddTaunt, BtnRemSpread, BtnRemMissile, BtnRemFan, BtnRemTaunt;
    public Text TxtSpread, TxtMissile, TxtFan, TxtTaunt;
    public Image ThreatLevelMask;
    public UnityEvent ShowTutorial;
    public void SaveData()
    {
        PlayerPrefs.SetString("SaveData", JsonUtility.ToJson(currentGameState));

    }
    public void LoadLastSave()
    {
        if (PlayerPrefs.HasKey("SaveData"))
        {
            string saveDataJSON = PlayerPrefs.GetString("SaveData");
            currentGameState = JsonUtility.FromJson<GameData.GameState>(saveDataJSON);
        }
        else
        {
            currentGameState = new GameData.GameState();
        }

    }
    public void GoToGameScreen()
    {
        SaveData();
        SceneManager.LoadScene(GameScreen);
    }
    public GameData.GameState currentGameState;
    public void ChangeSpread(int n)
    {
        currentGameState.SpreadLevel = Mathf.Clamp(currentGameState.SpreadLevel + n, 0, 3);
        UpdateData();
    }
    public void ChangeMissile(int n)
    {
        currentGameState.MissileLevel = Mathf.Clamp(currentGameState.MissileLevel + n, 0, 3);
        UpdateData();
    }
    public void ChangeRearMissile(int n)
    {
        currentGameState.FanMissileLevel = Mathf.Clamp(currentGameState.FanMissileLevel + n, 0, 3);
        UpdateData();
    }
    public void ChangeOpenComms(int n)
    {
        currentGameState.OpenComms = Mathf.Clamp(currentGameState.OpenComms + n, 0, TauntLevels.Length - 1);
        UpdateData();
    }
    // Use this for initialization
    void Start () {
        LoadLastSave();
        UpdateData();
        BtnAddMissile.onClick.AddListener(() => { ChangeMissile(1); });
        BtnAddSpread.onClick.AddListener(() => { ChangeSpread(1); });
        BtnAddTaunt.onClick.AddListener(() => { ChangeOpenComms(1); });
        BtnAddFan.onClick.AddListener(() => { ChangeRearMissile(1); });
        BtnRemMissile.onClick.AddListener(() => { ChangeMissile(-1); });
        BtnRemSpread.onClick.AddListener(() => { ChangeSpread(-1); });
        BtnRemTaunt.onClick.AddListener(() => { ChangeOpenComms(-1); });
        BtnRemFan.onClick.AddListener(() => { ChangeRearMissile(-1); });

        if(currentGameState.TutorialComplete == false)
        {
            ShowTutorial.Invoke();
            currentGameState.TutorialComplete = true;
        }
    }
    public string[] TauntLevels;
    public AnimationCurve FillAmount;
    void UpdateData()
    {
        TxtFan.text = string.Format("{0}", currentGameState.FanMissileLevel);
        TxtMissile.text = string.Format("{0}", currentGameState.MissileLevel);
        TxtSpread.text = string.Format("{0}", currentGameState.SpreadLevel);
        TxtTaunt.text = TauntLevels[currentGameState.OpenComms];

        BtnAddMissile.interactable = currentGameState.MissileLevel < 3;
        BtnAddSpread.interactable = currentGameState.SpreadLevel < 3;
        BtnAddTaunt.interactable = currentGameState.OpenComms < TauntLevels.Length - 1;
        BtnAddFan.interactable = currentGameState.FanMissileLevel < 3;

        BtnRemMissile.interactable = currentGameState.MissileLevel > 0;
        BtnRemSpread.interactable = currentGameState.SpreadLevel > 0;
        BtnRemTaunt.interactable = currentGameState.OpenComms > 0;
        BtnRemFan.interactable = currentGameState.FanMissileLevel > 0;

        ThreatLevelMask.fillAmount = FillAmount.Evaluate(currentGameState.ThreatLevel / 15f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
