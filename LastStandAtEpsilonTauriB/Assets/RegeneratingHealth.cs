﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class RegeneratingHealth : MonoBehaviour, IDamageable {
    public List<Vulnerability> ShieldVulnerabilities, HullVulnerabilities;
    public int MaxShield = 1000;
    public int MaxHull = 1000;
    public float TimeToRegenerate;
    public float RegenerationRate;
    public FloatEvent OnHull01, OnShield01;
    public IntEvent OnHull, OnShield;
    public IntEvent OnMaxHull, OnMaxShield;
    public bool IsDead = false;
    public GameObject SpawnOnDeath;
    public float DeathDelay = 3;
    public UnityEvent OnPreDeath, OnDeath;
    private float Shield, Hull;
    private float RegenerationCooldown;
    public UnityEvent OnShieldDown, OnHullRegen;
    private bool hullIsRegenerating = false;
    private void OnEnable()
    {
        Regenerate();
    }
    public void Regenerate() { 
        Shield = MaxShield;
        Hull = MaxHull;
        UpdateHPEvents();
    }
    void UpdateHPEvents()
    {
        Shield = Mathf.Clamp(Shield, 0, MaxShield);
        OnMaxHull.Invoke(MaxHull);
        OnMaxShield.Invoke(MaxShield);
        OnHull.Invoke((int)Hull);
        OnHull01.Invoke(Hull / (float)MaxHull);
        OnShield.Invoke((int)Shield);
        OnShield01.Invoke(Shield / (float)MaxShield);
        if(Hull <= 0)
        {
            if(!IsDead)
            StartCoroutine(DoDeath());
        }
    }
    public void Update()
    {
        if (IsDead) return;
        if(RegenerationCooldown <= 0 && Shield < MaxShield)
        {
            if (!hullIsRegenerating) OnHullRegen.Invoke();
                hullIsRegenerating = true;
            Shield += RegenerationRate * Time.deltaTime;
            UpdateHPEvents();
        }
        if (RegenerationCooldown > 0) RegenerationCooldown -= Time.deltaTime;
    }
    public bool TakeDamage(int damage, DamageType type)
    {
        if (IsDead) return false;
        RegenerationCooldown = TimeToRegenerate;
        hullIsRegenerating = false;
        if (Shield > 0)
        {
            var multiplier = ShieldVulnerabilities.FirstOrDefault(x => x.type == type);
            Shield -= damage * (multiplier != null ? multiplier.Multiplier : 1);
            //
            if(Shield <= 0 ) OnShieldDown.Invoke();
        } else
        {
            var multiplier = HullVulnerabilities.FirstOrDefault(x => x.type == type);
            Hull -= damage * (multiplier != null ? multiplier.Multiplier : 1); 
        }
        UpdateHPEvents();
        return true;
    }
    private IEnumerator DoDeath()
    {
        var spawned = PoolTracker.Get(SpawnOnDeath);
        spawned.transform.position = this.transform.position;
        spawned.SetActive(true);
        IsDead = true;
        OnPreDeath.Invoke();
        yield return new WaitForSeconds(DeathDelay);
        OnDeath.Invoke();
    }
    
}
