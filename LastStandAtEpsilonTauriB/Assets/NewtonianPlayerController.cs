﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NewtonianPlayerController : MonoBehaviour {
    public string RotationAxis = "Horizontal";
    public string AccelerationAxis = "Vertical";
    public string DeadStopButton = "Jump";
    public float BoostSpeed = 1.0f;
    public float RetroSpeed = 1.0f;
    public float RotationSpeed = 1.0f;
    public float RotationSpeedWithBrake = 1.0f;
    public float MaxSpeed = 20;
    
    public Camera WrapCamera;
    public float ScreenPadding;
    public Vector2 Velocity;
    public Vector3 ScreenPosition;
    public INewtonianAnimator Animator;
    public UnityEvent OnLoop; 
    private void Start()
    {
        Animator = this.gameObject.GetComponentInChildren<INewtonianAnimator>();
    }
    // Update is called once per frame
    void Update () {
        float RotationDirection = Input.GetAxis(RotationAxis);
        float AccelerationDirection = Input.GetAxis(AccelerationAxis);
        bool DeadStop = Input.GetButton(DeadStopButton);
        if(RotationDirection != 0)
        {
            this.transform.Rotate(Vector3.back, RotationDirection * (DeadStop? RotationSpeedWithBrake: RotationSpeed) * Time.deltaTime);
        }
        if (AccelerationDirection > 0)
        {
            Velocity += (Vector2)(this.transform.up * BoostSpeed * Time.deltaTime);
        }
        if (DeadStop)
        {
            Velocity -= Velocity.normalized * RetroSpeed * Time.deltaTime;
        } else if(AccelerationDirection < 0)
        {
            Velocity -= (Vector2)(this.transform.up * RetroSpeed * Time.deltaTime);

        }
        Velocity = Vector2.ClampMagnitude(Velocity, MaxSpeed);
        
        Animator.speed = AccelerationDirection;
        Animator.rotation = RotationDirection;
    }
    void FixedUpdate()
    {
        this.transform.Translate(Velocity * Time.fixedDeltaTime, Space.World);
        WrapToCamera();
    }
    Vector2 position;
    void WrapToCamera()
    {
        if(this.gameObject.WrapToCamera(WrapCamera,out position,ScreenPadding))
        {
            foreach (var dupe in this.GetComponentsInChildren<Duplicator>())
            {

                dupe.Duplicate();
            }
            OnLoop.Invoke();
            this.transform.position = position;
        }
    }
    
}
