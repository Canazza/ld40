﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour
{
    public AudioMixer mixer;
    public string MasterVolume = "MasterVolume";
    public string MusicVolume = "MusicVolume";
    public string SFXVolume = "SFXVolume";
    public AnimationCurve Volume01;
    public GameData.GameState currentGameState;
    public string NewGameScreen = "Game", ContinueGameScreen = "UpgradeScreen";
    public FloatEvent OnMasterVolume01, OnMusicVolume01, OnSFXVolume01;
    private void Start()
    {
        HasNoContinueGame.Invoke(PlayerPrefs.HasKey("SaveData"));
        updateVolumeLevels();
    }
    public void NewGame()
    {

        PlayerPrefs.SetString("SaveData", JsonUtility.ToJson(new GameData.GameState()));
        SceneManager.LoadScene(NewGameScreen);
    } 
    public void ContinueGame()
    {
        SceneManager.LoadScene(ContinueGameScreen);
    }
    public BoolEvent HasNoContinueGame;
    void updateVolumeLevels()
    {
        mixer.SetFloat(MasterVolume, Volume01.Evaluate(PlayerPrefs.GetFloat(MasterVolume,0.8f)));
        mixer.SetFloat(MusicVolume, Volume01.Evaluate(PlayerPrefs.GetFloat(MusicVolume, 0.8f)));
        mixer.SetFloat(SFXVolume, Volume01.Evaluate(PlayerPrefs.GetFloat(SFXVolume, 0.8f)));
        OnMasterVolume01.Invoke(PlayerPrefs.GetFloat(MasterVolume, 0.8f));
        OnMusicVolume01.Invoke(PlayerPrefs.GetFloat(MusicVolume, 0.8f));
        OnSFXVolume01.Invoke(PlayerPrefs.GetFloat(SFXVolume, 0.8f));
    }
    public void SetVolumeLevel(float t)
    {
        if (PlayerPrefs.GetFloat(MasterVolume, 0.8f) != t)
        {
            PlayerPrefs.SetFloat(MasterVolume, t);
            updateVolumeLevels();
        }
    }
    public void SetMusicLevel(float t)
    {
        if (PlayerPrefs.GetFloat(MusicVolume, 0.8f) != t)
        {
            PlayerPrefs.SetFloat(MusicVolume, t);
            updateVolumeLevels();
        }
    }
    public void SetSFXLevel(float t)
    {
        if (PlayerPrefs.GetFloat(SFXVolume, 0.8f) != t)
        {
            PlayerPrefs.SetFloat(SFXVolume, t);
            updateVolumeLevels();
        }
    }
}
