﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ThreatSpawner : MonoBehaviour {
    [System.Serializable]
    public class ThreatLevel
    {
        public int Level;
        public float StartSpawningAfterSeconds;
        public int TotalNumberToSpawn;
        public bool IsCompleted(int threatLevel)
        {
            if (this.Level > threatLevel) return true;
            if (TotalNumberToSpawn <= 0) return true;
            return false;
        }
        public List<GameObject> Spawned;
        public bool Spawn(GameObject Prefab, int threatLevel, float currentWaveTime, Transform[] locations)
        {
            if (this.Level > threatLevel) return false;
            if (currentWaveTime < StartSpawningAfterSeconds) return false;
            if (TotalNumberToSpawn <= 0) return false;
            var loc = locations[Random.Range(0, locations.Length)];
            var spawned = PoolTracker.Get(Prefab);
            spawned.transform.position = loc.position;
            spawned.transform.rotation = loc.rotation;
            spawned.SetActive(true);
            Spawned.Add(spawned);
            TotalNumberToSpawn--;
            return true;
        }
    }
    public GameObject Prefab;
    public List<ThreatLevel> Levels;
    public Transform[] Locations;
    public float SpawnInterval = 4;
    public float TimeBetweenLevelSpawns = 0.3f;
    private void OnEnable()
    {
        StartCoroutine(DoSpawns());
    }
    public bool IsComplete
    {
        get
        {
            var threatLevel = GameData.CurrentGameState.ThreatLevel;
            return Levels.All(x => x.IsCompleted(threatLevel));
        }
    }
    public IEnumerator DoSpawns()
    {
        yield return new WaitForSeconds(2); //Don't spawn until 2s game time has passed
        while (true)
        {
            foreach (var level in Levels)
            {
                if (level.Spawn(Prefab, GameData.CurrentGameState.ThreatLevel, Time.timeSinceLevelLoad, Locations))
                {
                    yield return new WaitForSeconds(TimeBetweenLevelSpawns);
                }
            }
            yield return new WaitForSeconds(SpawnInterval);
        }
    }
    private void OnDrawGizmosSelected()
    {
        foreach(var t in Locations)
        {
            Gizmos.DrawWireCube(t.position, Vector3.one / 2);
        }
    }
}
