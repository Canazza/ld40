﻿using UnityEngine;
using System.Collections;

public static class Utilities 
{
    public static bool WrapToCamera(this GameObject _this, Camera WrapCamera, out Vector2 WorldPosition, float padding = 0)
    {
        WorldPosition = new Vector2();
        if (WrapCamera == null) return false;
        var ScreenPosition = WrapCamera.WorldToViewportPoint(_this.transform.position);
        bool change = false;
        if (ScreenPosition.x < -padding)
        { 
            ScreenPosition.x = 1 + padding;// + ScreenPosition.x;
            change = true;
        }  else 
        if (ScreenPosition.x > 1 + padding)
        {
            ScreenPosition.x = - padding;// ScreenPosition.x - (1 + padding);
            change = true;
        }
        if (ScreenPosition.y < - padding)
        {
            ScreenPosition.y = 1 + padding + ScreenPosition.y;
            change = true;
        } else
        if (ScreenPosition.y > 1 + padding)
        {
            ScreenPosition.y = ScreenPosition.y - (1 + padding);
            change = true;
        } 
            WorldPosition = WrapCamera.ViewportToWorldPoint(ScreenPosition);
        return change;
    }
}
