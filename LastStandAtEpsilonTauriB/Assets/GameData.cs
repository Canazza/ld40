﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameData : MonoBehaviour {
    [System.Serializable]
    public class GameState
    {
        public bool TutorialComplete = false;
        public int Cash = 0;
        public int Score = 0;
        public int LaserLevel = 1;
        public int SpreadLevel = 0;
        public int MissileLevel = 0;
        public int FanMissileLevel = 0;
        public int ShieldLevel = 20;
        public int HullLevel = 20;
        public int OpenComms = 0;
        public int ThreatLevel
        {
            get
            {
                if (!TutorialComplete) return 0;
                return LaserLevel + SpreadLevel + MissileLevel + FanMissileLevel + OpenComms;
            }
        }

    }
    public string UpgradeScreen = "UpgradeScreen";
    public RegeneratingHealth PlayerHealth;
    public Spawner FrontLaserData, SpreadLaserData, MissileBatteryData, FanMissileBatteryData;
    public GameObject[] FrontLaser;
    public GameObject[] SpreadLevelOne, SpreadLevelTwo, SpreadLevelThree;
    public GameObject[] MissileBattery, FanMissileBattery;
    public GameState currentGameState;
    public ThreatSpawner[] ThreatSpawners;
    public bool GameActive = true;
    public UnityEvent OnRoundComplete;
    private static GameData _instance;
    public static GameState CurrentGameState
    {
        get {
            if (_instance == null) _instance = GameObject.FindObjectOfType<GameData>();
            return _instance.currentGameState;
        }
    }
    void Start()
    {
        LoadLastSave(); 
        UpdateGameState();
    }
    void Update()
    {
        if (GameActive && IsRoundComplete())
        {
            GameActive = false;
            OnRoundComplete.Invoke();
            Invoke("LevelComplete", 4);
        }

    }
    bool IsRoundComplete()
    {
        foreach(var spawner in ThreatSpawners)
        {
            if (!spawner.IsComplete) { Debug.Log(spawner.name + "incomplete", spawner); return false; }
        }
        return ComputerControlled.All.Count(x => x.activeSelf == true) <= 0;
    }
    public void LoadLastSave()
    {
        if (PlayerPrefs.HasKey("SaveData"))
        {
            string saveDataJSON = PlayerPrefs.GetString("SaveData");
            currentGameState = JsonUtility.FromJson<GameState>(saveDataJSON);
            UpdateGameState();
        }
        else
        {
            NewGame();
        }

    }
    public void GameOver()
    {
        LoadLastSave();
        GoToUpgradeScreen();
    }
    public void LevelComplete()
    {
        SaveData();
        GoToUpgradeScreen();
    }
    public void GoToUpgradeScreen()
    {
        SceneManager.LoadScene(UpgradeScreen);
    }
    public void NewGame()
    {
        currentGameState = new GameState()
        {

        };
        UpdateGameState();
    }
    public void UpdateGameState()
    {
        PlayerHealth.MaxShield = currentGameState.ShieldLevel;
        PlayerHealth.MaxHull = currentGameState.HullLevel;
        PlayerHealth.Regenerate();
        foreach (var item in FrontLaser)
        {
            item.SetActive(currentGameState.LaserLevel >= 1);
        }
        foreach (var item in MissileBattery)
        {
            item.SetActive(currentGameState.MissileLevel >= 1);
        }
        foreach (var item in FanMissileBattery)
        {
            item.SetActive(currentGameState.FanMissileLevel >= 1);
        } 
        foreach (var item in SpreadLevelOne)
        {
            item.SetActive(currentGameState.SpreadLevel >= 1);
        }
        foreach (var item in SpreadLevelTwo)
        {
            item.SetActive(currentGameState.SpreadLevel >= 2);
        }
        foreach (var item in SpreadLevelThree)
        {
            item.SetActive(currentGameState.SpreadLevel >= 3);
        }
        switch (currentGameState.LaserLevel)
        {
            case 3:
                FrontLaserData.ReloadTime = 0.2f;
                break;
            case 2:
                FrontLaserData.ReloadTime = 0.3f;
                break;
            default:
                FrontLaserData.ReloadTime = 0.4f;
                break;
        }
        switch (currentGameState.SpreadLevel)
        {
            case 3:
                SpreadLaserData.ReloadTime = 0.6f;
                break;
            case 2:
                SpreadLaserData.ReloadTime = 0.8f;
                break;
            default:
                SpreadLaserData.ReloadTime = 1f;
                break;
        }
        switch (currentGameState.MissileLevel)
        {
            case 3:
                MissileBatteryData.RefireTime = 0.2f;
                MissileBatteryData.ReloadTime = 2f;
                break;
            case 2:
                MissileBatteryData.RefireTime = 0.3f;
                MissileBatteryData.ReloadTime = 3f;
                break;
            default:
                MissileBatteryData.RefireTime = 0.4f;
                MissileBatteryData.ReloadTime = 3f;
                break;
        }
        switch (currentGameState.FanMissileLevel)
        {
            case 3:
                FanMissileBatteryData.RefireTime = 0.1f;
                FanMissileBatteryData.ReloadTime = 1f;
                break;
            case 2:
                FanMissileBatteryData.RefireTime = 0.2f;
                FanMissileBatteryData.ReloadTime = 2f;
                break;
            default:
                FanMissileBatteryData.RefireTime = 0.3f;
                FanMissileBatteryData.ReloadTime = 2f;
                break;
        }
    }
    private static AudioSource _source;
    internal static void PlayClip(AudioClip clip)
    {
        if (_source == null) _source = GameObject.FindGameObjectWithTag("AudioOneShot").GetComponent<AudioSource>();
        _source.PlayOneShot(clip);
    }

    public void SaveData()
    {
        PlayerPrefs.SetString("SaveData", JsonUtility.ToJson(currentGameState));

    }
}
