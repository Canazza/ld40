﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFormatter : MonoBehaviour {
    private Text text;
    public float scale = 1;
    private string Format;
    void Awake()
    {
        text = this.GetComponent<Text>();
        Format = text.text;
    }
    public void Set(float n)
    {
        text.text = string.Format(Format, n * scale);
    }
    public void Set(int n)
    {
        text.text = string.Format(Format, n * scale);
    }
}
