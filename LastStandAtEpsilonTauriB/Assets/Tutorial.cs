﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial : MonoBehaviour {
    public List<GameObject> Steps;
    public UnityEvent OnTutorialStart, OnTutorialEnd;
    public bool NextStep;
    public void GoNextStep() { NextStep = true; }
    public int ShowStep;
    public UnityEvent OnShowStep;
    private void OnEnable()
    {
        StartCoroutine(DoTutorial());
    }
    public IEnumerator DoTutorial()
    {
        NextStep = false;
        Steps.ForEach(x => x.SetActive(false));
        OnTutorialStart.Invoke();
        yield return null;
        int n = 0;
        foreach(var step in Steps)
        {
            NextStep = false;
            Steps.ForEach(x => x.SetActive(false));
            step.SetActive(true);
            while (!NextStep)
            {
                yield return null;
            }
            n++;
            if (n == ShowStep) OnShowStep.Invoke();
                
        }
        OnTutorialEnd.Invoke();
    } 
}
