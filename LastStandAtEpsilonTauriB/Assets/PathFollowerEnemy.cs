﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PathFollowerEnemy : ComputerControlled {
    public Vector2[] Path;
    public int pathIndex = 0;
    public float Distance;
    public float Speed;
    public UnityEvent OnPathEnd;
    public bool LoopPath;
    private Vector2 StartPosition;
    public bool TrackPlayer;
    private Transform Player;
    public new void OnEnable()
    {
        StartPosition = this.transform.position;
        pathIndex = 0;
        if (Player == null) Player = FindObjectOfType<NewtonianPlayerController>().transform;
    }
    Vector2 diff;
    public void Update()
    {
        if (TrackPlayer)
        {
            diff = Player.position - this.transform.position;

        }
        else
        {
            diff = (StartPosition + Path[pathIndex]) - (Vector2)this.transform.position;

        }
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        //this.transform.LookAt(Path[pathIndex]);
    }
    private void OnDrawGizmosSelected()
    {
        for (int i = 1; i < Path.Length; i++)
        {
            if (!Application.isPlaying)
            {
                Gizmos.DrawLine(Path[i - 1] + (Vector2)this.transform.position, Path[i] + (Vector2)this.transform.position);
            } else
            {
                Gizmos.DrawLine(Path[i - 1] + StartPosition, Path[i] + StartPosition);
            }
        }
        if(LoopPath)
        {
            if (!Application.isPlaying)
            {
                Gizmos.DrawLine(Path[0] + (Vector2)this.transform.position, Path[Path.Length - 1] + (Vector2)this.transform.position);
            }
            else
            {
                Gizmos.DrawLine(Path[0] + StartPosition, Path[Path.Length - 1] + StartPosition);
            }

        }
    }
    public void FixedUpdate()
    {
        this.transform.position = Vector2.MoveTowards(this.transform.position, StartPosition+ Path[pathIndex], Speed * Time.fixedDeltaTime);
        if (Vector2.Distance(this.transform.position, StartPosition + Path[pathIndex]) < Distance) pathIndex++;
        if(pathIndex >= Path.Length)
        {
            pathIndex = 0;
            if (!LoopPath)
            {
                this.transform.position = StartPosition;
            }
            OnPathEnd.Invoke();
        }
    }
}
