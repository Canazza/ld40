﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bullet : MonoBehaviour {
    public LayerMask CheckMask;
    public float Radius = 0.2f;
    public float Speed = 10;
    public float Life = 1;
    private float LifeLeft = 0;
    public int Damage = 1;
    public float LifeLostOnHit = 1;
    public UnityEvent OnLifetimeEnded;
    public Vector2 AdditionalVelocity;
    public DamageType TypeOfDamage = DamageType.Laser;
    
	// Use this for initialization

    private void OnEnable()
    {
        LifeLeft = Life;
    }
    private void Update()
    { 
        LifeLeft -= Time.deltaTime;
        if(LifeLeft <= 0)
        {
            OnLifetimeEnded.Invoke();
        }
    }
    private void FixedUpdate()
    {
        this.transform.Translate(Vector2.up * Speed * Time.fixedDeltaTime, Space.Self);
        this.transform.Translate(AdditionalVelocity * Time.fixedDeltaTime, Space.World);
        if (LifeLeft > 0)
        {
            var hit = Physics2D.OverlapCircle(this.transform.position, this.Radius, this.CheckMask);
            if (hit)
            {
                var dmg = hit.GetComponent<IDamageable>();
                dmg.TakeDamage(Damage,TypeOfDamage);
                LifeLeft -= LifeLostOnHit;
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(this.transform.position, Radius);
    }
}
